import numpy as np
from pymobility.models.mobility import random_waypoint
from scipy import spatial


def PositioningParameters(bottomLeft, bottomRight, topLeft, topright):
    cols=np.linspace(bottomLeft[0], bottomRight[0], num=3)
    rows=np.linspace(bottomLeft[1], topLeft[1], num=3)
    return cols, rows


def GridMatrix(cols, rows, GridList, AllGridsXY):
 for j in range(len(rows)-1):
               for i in range(len(cols)-1):
                   #Grid=np.append(Grid, np.array([ [cols[i], rows[j]],  [cols[i],rows[j+1]], [cols[i+1],rows[j+1]], [cols[i+1],rows[j]] ]))
                   Grid=np.array([ cols[i], rows[j], cols[i+1],rows[j], cols[i],rows[j+1], cols[i+1],rows[j+1] ])
                   AllGridsXY=np.append(AllGridsXY,Grid,axis=0)
                   GridList=np.vstack((GridList, Grid))
 return GridList, AllGridsXY

                  

def userDistribution(UENum, bottomLeft, bottomRight, topLeft, topRight):
    staticWaypoints = random_waypoint(UENum, dimensions=(bottomRight[0]-bottomLeft[0], topLeft[1]-bottomLeft[1]), velocity=(0.000001,0.00001), wt_max=1.0)
    #This will create a Randompoint (static positioning with velocity almost distributed between velcoity (min, max) units/step uniform distribution with 1.0 as max waitingtime of 1.0 steps)
    UE_Positions=next(staticWaypoints)
    return UE_Positions

def GridCentering(UENum, GridMatrixList, userDistMatrix,  GridCenters):
   
    for i in range(len(GridMatrixList)):
        GridXcenter=np.mean(np.array([GridMatrixList[i][0], GridMatrixList[i][2], GridMatrixList[i][4], GridMatrixList[i][6] ]))
        
        GridYcenter=np.mean(np.array([GridMatrixList[i][1], GridMatrixList[i][3], GridMatrixList[i][5], GridMatrixList[i][7]]))
        GridCenter=np.array([GridXcenter, GridYcenter])
        
        GridCenters=np.vstack((GridCenters, GridCenter))
    
    return GridCenters[1:]

#def UEnearestSector(userDistMatrix, GridMatrixList):
#    for i in range(len(GridMatrixList)):
#        for s in range(len(userDistMatrix)):
        
        
def main():
    #Defining the Parameters
    bottomLeft = (0,0)
    bottomRight = (10, 0)
    topLeft = (0, 10)
    topRight = (10, 10)
    GridList=np.array([bottomLeft[0], bottomLeft[1], bottomRight[0], bottomRight[1], topLeft[0], topLeft[1], topRight[0], topRight[1]])
    AllGridsXY=[]
    UENum=20
    
    #GridList=np.empty(shape=(4,2), dtype=int)
    
    #defining the executable functions
    Positions=PositioningParameters(bottomLeft, bottomRight, topLeft, topRight)
    cols=Positions[0]
    rows=Positions[1]
    GridMatrixList, AllGridsXYStack=GridMatrix(cols, rows, GridList, AllGridsXY)
    print(AllGridsXYStack.reshape(np.int(len(AllGridsXYStack)/2), 2))
    userDistMatrix=userDistribution(UENum, bottomLeft, bottomRight, topLeft, topRight)
    GridCenteringMatrix=GridCentering(UENum, GridMatrixList, userDistMatrix, np.empty([1,2], dtype=float))
    
    return
 

if __name__ =="__main__":
    main()




