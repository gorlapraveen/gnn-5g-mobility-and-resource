import numpy as np
import matplotlib.pyplot as plt
import torch
from scipy.spatial import distance_matrix
from tqdm import tqdm

#Channel class and testing

def pathloss(d, d0, gamma):
    """
    Calculate simplified path-loss model.

    Args:
        d: The distance. Can be a matrix - this is an elementwise operation.
        d0: Reference distance.
        gamma: Path loss exponent.

    Returns: Pathloss value.

    """
    return (d0/d)**gamma

#Testing the function with d varying betwwn 1m to 100 m and Potting  the pathloss with linear and algorithmic scale

Pt=np.zeros(100)
dist = np.arange(100)
Pt=pathloss(dist+1, 1, 2.2)
plt.figure()
plt.plot(Pt)
plt.ylabel('Pathloss')
plt.xlabel('Distance')
plt.savefig('Pathloss.pdf')
plt.show()
# # Plot in logarithmic scale
plt.figure()
plt.plot(np.log(dist), np.log(Pt) )
plt.ylabel('log of Pathloss')
plt.xlabel('log of Distance')
plt.savefig('log-of-Pathloss.pdf')
plt.show()


#Simulating the fading channel incorporating Pathloss and fading for Q channels realization over a distance of do to d

def fading_channel(d, d0, gamma, s, Q):
    Pt=(d0/d)**gamma
    h_til=np.random.exponential(s, size=(1, Q))
    h = Pt * h_til / s
    return h

Q=100
Pt=np.zeros(100)
dist = np.arange(100)
h_sim = np.zeros((100, Q))
for dis in range(100):
    h_sim[dis,:]=fading_channel(dis+1, 1, 2.2, 2, 100)

h_mean = np.mean( h_sim, axis = 1)
h_var = np.var( h_sim, axis = 1 )
plt.figure()
plt.errorbar(dist, h_mean, h_var)
plt.ylabel('h')
plt.xlabel('Distance')
plt.show()

#Building Capacity with Fading channel mechanism 

def fading_channel_capacity (h, p):
    N0=1e-6
    cap=np.log(1+h*p/N0)
    
    return cap 

Capacity=fading_channel_capacity (h_sim, 0.05)
Capacity_mean=np.mean(Capacity, axis=1)
Capacity_var=np.var(Capacity, axis=1)
dist=np.arange(100)
print(h_var)
plt.figure()
plt.errorbar(dist, Capacity_mean, Capacity_var)
plt.ylabel('Capacity')
plt.xlabel('Distance')
plt.show()


#Positioning with pasthloss with n number of nodes

def positioning(Tn, Rn, Ax, Ay, Ac):
    Tx_Positioning=np.hstack((np.random.uniform(0,Ax, (Tn-1))), np.random.uniform(0, Ay, (Rn,1))))
    #np.hstack is used of stacking, Stack:Join a sequence of arrays along a new axis. ex: a = np.array([[1],[2],[3]]) b = np.array([[2],[3],[4]]), np.stack((a,b)) gives array([[1, 2], [2, 3], [3, 4]])
    
    Rx_distance=np.random.uniform(0, Ac, )

